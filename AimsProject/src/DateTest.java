import java.util.ArrayList;
import java.util.List;

public class DateTest {

	public static void main(String[] args) {
//		MyDate demo1 = new MyDate();
//		MyDate demo2 = new MyDate(30, 2, 2019);
//		MyDate demo3 = new MyDate("Jul-12-1999");
//
//		demo1.print();
//		demo2.print();
//		demo3.print();
		
//		MyDate demo4 = new MyDate(31,12,1999);
//		demo4.print("yyyy-MM-dd");
		
		List<MyDate> list = new ArrayList<MyDate>();
		list.add(new MyDate(11, 7, 2020));
		list.add(new MyDate(8, 8, 2020));
		list.add(new MyDate(8, 8, 2021));
		list.add(new MyDate(12, 7, 1999));
		list.add(new MyDate(22, 6, 2019));
		list.add(new MyDate(-1, 4, 2020));
		list.add(new MyDate(33, 4, 2020));
		
		DateUtils.sort(list);
		
		list.forEach(MyDate::print);
		

	}

}
