package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
	
	private List<String> authors = new ArrayList<String>();
	
	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public Book(int id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		this.authors.forEach(author -> {
			if (author.equals(authorName)) {
				System.out.println("The author " + authorName + " is present in the list!");
				return;
			}
		});
		this.authors.add(authorName);
		return;
		
	}
	
	public void removeAuthor(String authorName) {
		this.authors.forEach(author ->{
			if (author.equals(authorName)) {
				this.authors.remove(authorName);
				return;
			}
		});
		System.out.println("The author " + authorName + " is not already in the list!");
		return;
		
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "Book [authors=" + authors + ", id=" + getId() + ", title=" + getTitle() + ", category="
				+ getCategory() + ", cost=" + getCost() + "]";
	}
	
	

}
