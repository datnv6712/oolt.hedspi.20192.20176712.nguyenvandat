package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Media {
	
	private String director;
	private int length;
	
	public boolean search(String title) {
		return this.getTitle().toLowerCase().contains(title.toLowerCase());
	}

	public DigitalVideoDisc(int id, String title) {
		super();
		this.setId(id);
		this.setTitle(title);
	}

	public DigitalVideoDisc(int id, String title, String category) {
		super();
		this.setId(id);
		this.setTitle(title);
		this.setCategory(category);
	}

	public DigitalVideoDisc(int id, String title, String category, String director) {
		super();
		this.setId(id);
		this.setTitle(title);
		this.setCategory(category);
		this.director = director;
	}

	public DigitalVideoDisc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, cost);
		this.director = director;
		this.length = length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "DigitalVideoDisc [director=" + director + ", length=" + length + ", id=" + getId()
				+ ", title=" + getTitle() + ", category=" + getCategory() + ", cost=" + getCost() + "]";
	}
	
}
