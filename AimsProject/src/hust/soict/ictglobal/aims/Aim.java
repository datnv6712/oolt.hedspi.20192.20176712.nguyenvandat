package hust.soict.ictglobal.aims;

import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class Aim {
	public static void main(String[] args) {
		try {
			Order demo = null;
			int choice = 0;
			Scanner keyboard = new Scanner(System.in);

			do {
				showMenu();
				System.out.print("Select: ");
				choice = keyboard.nextInt();

				switch (choice) {
				case 1:
					demo = new Order();
					System.out.println("New order created!");
					break;

				case 2:
					if (demo == null) {
						demo = new Order();
						System.out.println("New order created!");
					}
					
					demo.addMedia(
							new DigitalVideoDisc(1, "Star World", "Science Fiction", "George Lucas", 124, 24.95f));
					demo.addMedia(new Book(2, "Lion King", "Animation", 19.95f, List.of("Roger Allers")));
					demo.addMedia(new DigitalVideoDisc(3, "Avatar", "Science Fiction", "James Cameron", 162, 29.95f));
					demo.addMedia(new DigitalVideoDisc(4, "Whiplash", "Drama", "Damien Chazelle", 107, 15.95f));

					demo.addMedia(new Book(5, "Interstellar", "Science Fiction", 22.99f, List.of("Christopher Nolan")));

					demo.addMedia(new DigitalVideoDisc(6, "Aladdin", "Animation", "John Musker", 90, 18.99f));

					break;

				case 3:
					System.out.print("Enter item id: ");
					int toRemoveId = keyboard.nextInt();

					if (demo.removeMediaById(toRemoveId)) {
						System.out.println("Removed id - " + toRemoveId);
					} else {
						System.out.println("Id not found - " + toRemoveId);
					}
					break;

				case 4:
					demo.print();
					break;

				case 0:
					break;

				default:
					throw new IllegalArgumentException("Unexpected value: " + choice);
				}

			} while (choice != 0);

			keyboard.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");

	}

}
