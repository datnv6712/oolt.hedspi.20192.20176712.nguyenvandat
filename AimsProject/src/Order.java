
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 5;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private int qtyOrdered = 0;
	
	private MyDate dateOrder;
	
	private static final int MAX_LIMITTED_ORDERS = 3;
	
	private static int nbOrders = 0;
	
	public Order() throws Exception {
		if(nbOrders < MAX_LIMITTED_ORDERS) {
			nbOrders++;
		}
		else throw new Exception("The number of orders exceeds the limit!");
		
		this.dateOrder = new MyDate();
	}

	public MyDate getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(MyDate dateOrder) {
		this.dateOrder = dateOrder;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void getItemsOrdered() {
		for (int i = 0; i < itemsOrdered.length; i++) {
			System.out.println(itemsOrdered[i]);
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		
		if (this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered[this.qtyOrdered++] = disc;
			System.out.println("The disc \"" + disc.getTitle() + "\" has been added");
			return;
		}
		System.out.print("The order is almost full: ");
		System.out.println("The disc \"" + disc.getTitle() + "\" has not been added");
	}
	
//	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
//		for (DigitalVideoDisc disc : dvdList) {
//			this.addDigitalVideoDisc(disc);
//		}
//	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc ...dvdList) {
		for (DigitalVideoDisc disc : dvdList) {
			this.addDigitalVideoDisc(disc);
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		this.addDigitalVideoDisc(dvd1);
		this.addDigitalVideoDisc(dvd2);
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (int i = 0; i < this.qtyOrdered; i++) {
			if (disc.getTitle().equals(this.itemsOrdered[i].getTitle())) {
				this.qtyOrdered--;
				for (int j = i; j < this.qtyOrdered ; j++) {
					this.itemsOrdered[j] = this.itemsOrdered[j+1];
				}
			}
		}
		
	}
	
	public float totalCost() {
		float total = 0;
		
		for (DigitalVideoDisc item : itemsOrdered) {
			total += item.getCost();
		}
		
		return total;
	}
	
	public void print() {
		System.out.println("***");
		System.out.println("Date: " + this.dateOrder);
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.length; i++) {
			System.out.println( (i+1) + ". " + itemsOrdered[i]);
		}
		System.out.println("Total: " + totalCost());
	}

}
