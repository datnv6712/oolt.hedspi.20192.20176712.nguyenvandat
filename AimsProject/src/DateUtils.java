import java.util.Collections;
import java.util.List;

public class DateUtils {

	public static int compare(MyDate date1, MyDate date2) {
		if (date1.getYear() != date2.getYear())
			return date1.getYear() > date2.getYear() ? 1 : -1;

		if (date1.getMonth() != date2.getMonth())
			return date1.getMonth() > date2.getMonth() ? 1 : -1;

		if (date1.getDate() != date2.getDate())
			return date1.getDate() > date2.getDate() ? 1 : -1;
			
		return 0;
	}
	
	public static void sort(List<MyDate> list) {
		Collections.sort(list, DateUtils::compare);
	}

}
