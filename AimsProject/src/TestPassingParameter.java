
public class TestPassingParameter {
	
	public static void main(String[] args) {
		DigitalVideoDisc jungleDvd = new DigitalVideoDisc("Jungle");
		DigitalVideoDisc cinderellaDvd = new DigitalVideoDisc("Cinderella");

		System.out.println("---swap method---");
		swap(jungleDvd, cinderellaDvd);
		System.out.println("jungleDvd: " + jungleDvd.getTitle());
		System.out.println("cinderellaDvd: " + cinderellaDvd.getTitle());

		System.out.println("---changeTitle method---");
		changeTitle(jungleDvd, cinderellaDvd.getTitle());
		System.out.println("jungleDvd: " + jungleDvd.getTitle());
		System.out.println("cinderellaDvd: " + cinderellaDvd.getTitle());
		
	}
	
	public static void swap (Object obj1, Object obj2) {
		Object temp = obj1;
		obj1 = obj2;
		obj2 = temp;
	}
	
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc(oldTitle);
	}
	
	

}
