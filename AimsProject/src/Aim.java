
public class Aim {
	public static void main(String[] args) {
		Order demo;
		try {
			demo = new Order();

			demo.addDigitalVideoDisc(new DigitalVideoDisc("Star World", "Science Fiction", "George Lucas", 124, 24.95f),
					new DigitalVideoDisc("Lion King", "Animation", "Roger Allers", 87, 19.95f),
					new DigitalVideoDisc("Avatar", "Science Fiction", "James Cameron", 162, 29.95f),
					new DigitalVideoDisc("Whiplash", "Drama", "Damien Chazelle", 107, 15.95f));

			demo.addDigitalVideoDisc(
					new DigitalVideoDisc("Interstellar", "Science Fiction", "Christopher Nolan", 169, 22.99f),
					new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f));

			demo.print();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
