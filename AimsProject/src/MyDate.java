
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class MyDate {
	private int date, month, year;

	public MyDate() {
		Date today = new Date();
		date = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		month = Integer.parseInt(new SimpleDateFormat("MM").format(today));
		year = Integer.parseInt(new SimpleDateFormat("yyyy").format(today));
	}

	public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
		
		if (!validate()) System.out.println(this.date + "-" + this.month + "-" + this.year + " not valid");

		while (!validate() && this.date >27) {
			this.date--;
		}
		
		while (!validate() && this.date < 1) {
			this.date++;
		}
		
	}

	public MyDate(String input) {
		// e.g. "Jul-12-1999"
		SimpleDateFormat parser = new SimpleDateFormat("MMM-dd-yyyy");

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd");
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");

		try {
			this.date = Integer.parseInt(dateFormatter.format(parser.parse(input)));
			this.month = Integer.parseInt(monthFormatter.format(parser.parse(input)));
			this.year = Integer.parseInt(yearFormatter.format(parser.parse(input)));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		if (date > 0 && date < 32) {
			this.date = date;
			return;
		}
		System.out.println("Date not valid!");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month > 0 && month < 13) {
			this.month = month;
			return;
		}
		System.out.println("Month not valid!");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void setDate(String date) {
		date = date.toLowerCase();
		switch (date) {
		case "first": this.setDate(1); break;
		case "second": this.setDate(2); break;
		case "third": this.setDate(3); break;
		case "fourth": this.setDate(4); break;
		case "fifth": this.setDate(5); break;
		case "sixth": this.setDate(6); break;
		case "seventh": this.setDate(7); break;
		case "eighth": this.setDate(8); break;
		case "ninth": this.setDate(9); break;
		case "tenth": this.setDate(10); break;
		case "eleventh": this.setDate(11); break;
		case "twelfth": this.setDate(12); break;
		case "thirteenth": this.setDate(13); break;
		case "fourteenth": this.setDate(14); break;
		case "fifteenth": this.setDate(15); break;
		case "sixteenth": this.setDate(16); break;
		case "seventeenth": this.setDate(17); break;
		case "eighteenth": this.setDate(18); break;
		case "nineteenth": this.setDate(19); break;
		case "twentieth": this.setDate(20); break;
		case "twenty first": this.setDate(21); break;
		case "twenty second": this.setDate(22); break;
		case "twenty third": this.setDate(23); break;
		case "twenty fourth": this.setDate(24); break;
		case "twenty fifth": this.setDate(25); break;
		case "twenty sixth": this.setDate(26); break;
		case "twenty seventh": this.setDate(27); break;
		case "twenty eighth": this.setDate(28); break;
		case "twenty ninth": this.setDate(29); break;
		case "thirtieth": this.setDate(30); break;
		case "thirty first": this.setDate(31); break;
		
		default: System.out.println("Date not exists!"); break;
		}
	}

	public void setMonth(String month) {
		Date date;
		try {
			date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
		    Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		    this.setMonth(cal.get(Calendar.MONTH) + 1);
		    
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setYear(String year) {
		this.year = Integer.parseInt(year);
	}

	public boolean validate() {
		if (date == 31 && !(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12))
			return false;
		
		if (date==30 && month==2)
			return false;
		
		if (date==29 && month==2 && !isLeapYear())
			return false;
		
		return date >= 1 && date <= 31;
	}

	public boolean isLeapYear() {
		if (year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return true;
				else
					return false;
			} else
				return true;
		} else
			return false;
	}

	public void print() {
		System.out.println(this.date + "-" + this.month + "-" + this.year);
	}
	
	public void print(String pattern) {
		DateFormat formatter = new SimpleDateFormat(pattern);
		@SuppressWarnings("deprecation")
		Date data = new Date(this.year-1900, this.month-1, this.date);
		System.out.println(formatter.format(data));
	}

	public void accept() {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a date(e.g. \"Jul-12-1999\") : ");
		String input = keyboard.nextLine();

		SimpleDateFormat parser = new SimpleDateFormat("MMM-dd-yyyy");

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd");
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");

		try {
			this.date = Integer.parseInt(dateFormatter.format(parser.parse(input)));
			this.month = Integer.parseInt(monthFormatter.format(parser.parse(input)));
			this.year = Integer.parseInt(yearFormatter.format(parser.parse(input)));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		keyboard.close();

	}

	@Override
	public String toString() {
		return "[date:" + date + ", month:" + month + ", year:" + year + "]";
	}
	
	

}